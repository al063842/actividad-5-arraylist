package actividad.pkg5.arraylist;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Rodrigo Balan
 */
public class Actividad5ArrayList {

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);

        ArrayList<Integer> rgb = new ArrayList<Integer>();
        ArrayList<Integer> rgb2 = new ArrayList<Integer>();
        ArrayList<String> alfa = new ArrayList<String>();
        ArrayList<String> mors = new ArrayList<String>();
        ArrayList<String> Octavos = new ArrayList<String>();
        ArrayList<Integer> gol1 = new ArrayList<Integer>();
        ArrayList<String> Cuartos = new ArrayList<String>();
        ArrayList<Integer> gol2 = new ArrayList<Integer>();
        ArrayList<String> Semifinal = new ArrayList<String>();
        ArrayList<Integer> gol3 = new ArrayList<Integer>();
        rgb.add(90);
        rgb.add(150);
        rgb.add(255);
        System.out.println("Ejercicio 1");
        System.out.println(rgb);
        rgb2 = RGB(rgb);
        System.out.println(rgb2);
        alfa.add("A");
        alfa.add("B");
        alfa.add("C");
        alfa.add("D");
        alfa.add("E");
        alfa.add("F");
        alfa.add("G");
        alfa.add("H");
        alfa.add("I");
        alfa.add("J");
        alfa.add("K");
        alfa.add("L");
        alfa.add("M");
        alfa.add("N");
        alfa.add("O");
        alfa.add("P");
        alfa.add("Q");
        alfa.add("R");
        alfa.add("S");
        alfa.add("T");
        alfa.add("U");
        alfa.add("V");
        alfa.add("W");
        alfa.add("X");
        alfa.add("Y");
        alfa.add("Z");
        mors.add(".-");
        mors.add("-...");
        mors.add("-.-.");
        mors.add("-..");
        mors.add(".");
        mors.add("..-.");
        mors.add("--.");
        mors.add("....");
        mors.add("..");
        mors.add(".---");
        mors.add("-.-");
        mors.add(".-..");
        mors.add("--");
        mors.add("-.");
        mors.add("---");
        mors.add(".--.");
        mors.add("--.-");
        mors.add(".-.");
        mors.add("...");
        mors.add("-");
        mors.add("..-");
        mors.add("...-");
        mors.add(".--");
        mors.add("-..-");
        mors.add("-.--");
        mors.add("--..");

        System.out.println("------");
        System.out.println("Ejercicio 2");
        Actividad5ArrayList.Morse(mors, alfa);
        System.out.println("------");
        System.out.println("Ejercicio 3");
        System.out.println("Ingrese una palabra");
        String pal = sc.nextLine();
        List<Character> chars = new ArrayList<>();
        for (char ch : pal.toCharArray()) {
            chars.add(ch);
        }
        int asci;
        int i = 0;
        char cChar = chars.get(i);
        asci = (int) cChar;
        Actividad5ArrayList.ascii((ArrayList<Character>) chars, asci);
        System.out.println();
        Octavos.add("América");
        Octavos.add("Chivas");
        Octavos.add("Leon");
        Octavos.add("Cruz Azul");
        Octavos.add("Toluca");
        Octavos.add("Tigres");
        Octavos.add("Monterrey");
        Octavos.add("Pachuca");

        gol1.add(4);
        gol1.add(3);
        gol1.add(2);
        gol1.add(3);
        gol1.add(5);
        gol1.add(4);
        gol1.add(1);
        gol1.add(2);

        Cuartos.add("Pachuca");
        Cuartos.add("Monterrey");
        Cuartos.add("Leon");
        Cuartos.add("Toluca");

        gol2.add(4);
        gol2.add(2);
        gol2.add(8);
        gol2.add(3);

        Semifinal.add("Pachuca");
        Semifinal.add("Chivas");
        Semifinal.add("Tigres");
        Semifinal.add("Cruz Azul");

        gol3.add(2);
        gol3.add(7);
        gol3.add(6);
        gol3.add(5);
        System.out.println("-----");
        System.out.println("Ejercicio 4");
        Actividad5ArrayList.partidoF(Octavos, Cuartos, Semifinal, gol1, gol2, gol3);

    }

    public static ArrayList<Integer> RGB(ArrayList<Integer> listaR) {
        listaR.set(0, Math.abs(listaR.get(0) - 255));
        listaR.set(1, Math.abs(listaR.get(1) - 255));
        listaR.set(2, Math.abs(listaR.get(2) - 255));
        return listaR;
    }

    public static void Morse(ArrayList<String> alfa, ArrayList<String> mor) {

        System.out.println("Letras del alfabeto en Morse");

        for (int i = 0; i < 26; i++) {
            System.out.println(alfa.get(i) + " = " + mor.get(i));
        }

    }

    public static void ascii(ArrayList<Character> chars, int asciiValue) {

        Scanner sc = new Scanner(System.in);

        for (Character c : chars) {

            System.out.println("La siguiente letra " + (char) (c + 0) + " es la numero " + (c + 0) + " en codigo Morse");

        }
    }

    public static void partidoF(ArrayList<String> Octavos, ArrayList<String> Cuartos,
            ArrayList<String> Semifinal, ArrayList<Integer> gol1, ArrayList<Integer> gol2, ArrayList<Integer> gol3) {
        Scanner sc = new Scanner(System.in);
        if (gol1.get(0) > gol1.get(7)) {
            System.out.println("Resultados de los octavos de final");
            System.out.println(Octavos.get(0) + " es el ganador del partido vs " + Octavos.get(7) + " con una puntuacion de " + gol1.get(0) + " goles");
        } else {
            System.out.println(Octavos.get(7) + " es el ganador del partido vs " + Octavos.get(0) + " con una puntuacion de " + gol1.get(7) + " goles");
        }

        if (gol1.get(1) > gol1.get(6)) {
            System.out.println(Octavos.get(1) + " es el ganador del partido vs " + Octavos.get(6) + " con una puntuacion de " + gol1.get(1) + " goles");
        } else {
            System.out.println(Octavos.get(6) + " es el ganador del partido vs " + Octavos.get(1) + " con una puntuacion de " + gol1.get(6) + " goles");
        }

        if (gol1.get(2) > gol1.get(5)) {
            System.out.println(Octavos.get(2) + " es el ganador del partido vs " + Octavos.get(5) + " con una puntuacion de " + gol1.get(2) + " goles");
        } else {
            System.out.println(Octavos.get(5) + " es el ganador del partido vs " + Octavos.get(2) + " con una puntuacion de " + gol1.get(5) + " goles");
        }

        if (gol1.get(3) > gol1.get(4)) {
            System.out.println(Octavos.get(3) + " es el ganador del partido vs " + Octavos.get(4) + " con una puntuacion de " + gol1.get(3) + " goles");
        } else {
            System.out.println(Octavos.get(4) + " es el ganador del partido vs " + Octavos.get(3) + " con una puntuacion de " + gol1.get(4) + " goles");
        }

        System.out.println("-----");
        if (gol2.get(0) > gol2.get(3)) {
            System.out.println("Resultados de los cuartos de final");
            System.out.println(Cuartos.get(0) + " es el ganador del partido vs " + Cuartos.get(3) + " con una puntuacion de " + gol2.get(0) + " goles");
        } else {
            System.out.println(Cuartos.get(3) + " es el ganador del partido vs " + Cuartos.get(0) + " con una puntuacion de " + gol2.get(3) + " goles");
        }
        if (gol2.get(1) > gol2.get(2)) {
            System.out.println(Cuartos.get(1) + " es el ganador del partido vs " + Cuartos.get(2) + " con una puntuacion de " + gol2.get(1) + " goles");
        } else {
            System.out.println(Cuartos.get(2) + " es el ganador del partido vs " + Cuartos.get(1) + " con una puntuacion de " + gol2.get(2) + " goles");
        }

        System.out.println("-----");
        if (gol3.get(0) > gol3.get(3)) {
            System.out.println("Resultados de la semfinal");
            System.out.println(Semifinal.get(0) + " es el ganador del partido vs " + Semifinal.get(3) + " con una puntuacion de " + gol3.get(0) + " goles");
        } else {
            System.out.println(Semifinal.get(3) + " es el ganador del partido vs " + Semifinal.get(0) + " con una puntuacion de " + gol3.get(3) + " goles");
        }
        if (gol3.get(1) > gol3.get(2)) {
            System.out.println(Semifinal.get(1) + " es el ganador del partido vs " + Semifinal.get(2) + " con una puntuacion de " + gol3.get(1) + " goles");
        } else {
            System.out.println(Semifinal.get(2) + " es el ganador del partido vs " + Semifinal.get(1) + " con una puntuacion de " + gol3.get(2) + " goles");
        }

    }

}
